# Generic binary to 7segmentt display driver

It takes N-bit binary input and converts it to equivalent 7segment digits using double dabble algorithm written in VHDL.

If you want to know more detail click [here](https://32954.digital/blog/2021/06/19/binary-to-7segment)

Example is using src to displays numbers from 000... to `COUNT_TO` and then repeats.

# How to use

`BINARY_WIDTH`: number of bits in binary input.

`DISPLAY_NUMBER`: number of 7segment display digits.

`COMMON_ANODE`: type of 7segment display (common anode or common cathode).

### **The example has three more generics:**

`DEBOUNCE_CLOCK_DIVIDER`: divide input clock by 2<sup>N</sup> for clock of the module that debouches the button when you press it.

`MULTIPLEXING_CLOCK_DIVIDER`: divide input clock by 2<sup>N</sup> for 7segment display multiplexing speed.

`COUNT_TO`: maximum number that will be displayed on  7segment display.

# Schematics

<img width="100%" src="images/binary_to_7segment.svg">

<img width="100%" src="images/count_up_7segment.svg">

# References

- [Double dabble algorithm](https://en.wikipedia.org/w/index.php?title=Double_dabble&oldid=997863872)

- [Debouncer and ring counter: LBEbooks](https://www.youtube.com/user/LBEbooks)

- Generic Mux: [this one](https://codereview.stackexchange.com/questions/73708/vhdl-mux-in-need-of-generics) and [this one](https://stackoverflow.com/questions/34590157/generic-mux-and-demux-using-generics)
