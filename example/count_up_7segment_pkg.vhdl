LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

PACKAGE count_up_7segment_pkg IS
    COMPONENT debounce_clock_generator IS
        GENERIC (
            DEBOUNCE_CLOCK_DIVIDER : POSITIVE
        );
        PORT (
            reset_n, clock : IN STD_LOGIC;
            debounce_clock_out : OUT STD_LOGIC
        );
    END COMPONENT;
    ---------------------------------------------------------------------------
    COMPONENT multiplexing_clock_generator IS
        GENERIC (
            MULTIPLEXING_CLOCK_DIVIDER : POSITIVE
        );
        PORT (
            reset_n, clock : IN STD_LOGIC;
            multiplexing_clock_out : OUT STD_LOGIC
        );
    END COMPONENT;
    ---------------------------------------------------------------------------
    COMPONENT push_button_debounce IS
        PORT (
            reset_n, clock : IN STD_LOGIC;
            push_button : IN STD_LOGIC;
            debounced_push_button_out : OUT STD_LOGIC
        );
    END COMPONENT;
    ---------------------------------------------------------------------------
    COMPONENT ascending_binary_generator IS
        GENERIC (
            BINARY_WIDTH : POSITIVE;
            COUNT_TO : NATURAL
        );
        PORT (
            reset_n, count_clock : IN STD_LOGIC;
            binary_out : OUT STD_LOGIC_VECTOR((BINARY_WIDTH - 1) DOWNTO 0)
        );
    END COMPONENT;
    ---------------------------------------------------------------------------
    COMPONENT binary_to_7segment IS
        GENERIC (
            BINARY_WIDTH : POSITIVE;
            DISPLAY_NUMBER : POSITIVE;
            COMMON_ANODE : BOOLEAN
        );
        PORT (
            reset_n, multiplexing_clock : IN STD_LOGIC;
            binary_input : IN STD_LOGIC_VECTOR((BINARY_WIDTH - 1) DOWNTO 0);
            common_pins_out : OUT STD_LOGIC_VECTOR ((DISPLAY_NUMBER - 1) DOWNTO 0);
            seven_segment_out : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
        );
    END COMPONENT;
END PACKAGE;