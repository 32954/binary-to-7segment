LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY count_up_7segment_tb IS
    CONSTANT DEBOUNCE_CLOCK_DIVIDER : POSITIVE := 1;
    CONSTANT MULTIPLEXING_CLOCK_DIVIDER : POSITIVE := 1;
    CONSTANT BINARY_WIDTH : POSITIVE := 14;
    CONSTANT COUNT_TO : NATURAL := 9999;
    CONSTANT DISPLAY_NUMBER : POSITIVE := 4;
    CONSTANT COMMON_ANODE : BOOLEAN := true;

    CONSTANT CLOCK_PERIOD : TIME := 20 ns;
    CONSTANT RESET_DURATION : TIME := 100 ns;
    CONSTANT PUSH_BUTTON_PERIOD : TIME := 200 ns;
END ENTITY;

ARCHITECTURE testbench OF count_up_7segment_tb IS
    SIGNAL reset_n : STD_LOGIC := '0';
    SIGNAL push_button : STD_LOGIC := '0';
    SIGNAL clock : STD_LOGIC := '0';
    SIGNAL common_pins_out : STD_LOGIC_VECTOR ((DISPLAY_NUMBER - 1) DOWNTO 0);
    SIGNAL seven_segment_out : STD_LOGIC_VECTOR(6 DOWNTO 0);

    SIGNAL button_count : NATURAL;

BEGIN
    ---------------------------- DUT instantiation ----------------------------
    dut : ENTITY work.count_up_7segment GENERIC MAP (
        DEBOUNCE_CLOCK_DIVIDER => DEBOUNCE_CLOCK_DIVIDER,
        MULTIPLEXING_CLOCK_DIVIDER => MULTIPLEXING_CLOCK_DIVIDER,
        BINARY_WIDTH => BINARY_WIDTH,
        COUNT_TO => COUNT_TO,
        DISPLAY_NUMBER => DISPLAY_NUMBER,
        COMMON_ANODE => COMMON_ANODE
        )
        PORT MAP(
            reset_n => reset_n,
            push_button => push_button,
            clock => clock,
            common_pins_out => common_pins_out,
            seven_segment_out => seven_segment_out
        );
    --------------------------- stimuli generation ----------------------------
    clock <= NOT clock AFTER CLOCK_PERIOD / 2;
    reset_n <= '0', '1' AFTER RESET_DURATION;
    push_button <= NOT push_button AFTER PUSH_BUTTON_PERIOD;

    PROCESS
        ALIAS push_button_debounce IS << SIGNAL dut.debounced_push_button : STD_LOGIC >> ;
    BEGIN
        WAIT UNTIL rising_edge(push_button_debounce);
        button_count <= button_count + 1;
    END PROCESS;
    ---------------------------------- check ----------------------------------
    response_checker : PROCESS
        VARIABLE is_digit : BOOLEAN;
        VARIABLE common_pin : BOOLEAN;
    BEGIN
        WAIT FOR RESET_DURATION;

        CASE seven_segment_out IS
                --GFEDCBA
            WHEN "1000000" => is_digit := true; --0
            WHEN "1111001" => is_digit := true; --1
            WHEN "0100100" => is_digit := true; --2
            WHEN "0110000" => is_digit := true; --3
            WHEN "0011001" => is_digit := true; --4
            WHEN "0010010" => is_digit := true; --5
            WHEN "0000010" => is_digit := true; --6
            WHEN "1111000" => is_digit := true; --7
            WHEN "0000000" => is_digit := true; --8
            WHEN "0010000" => is_digit := true; --9
            WHEN OTHERS => is_digit := false;
        END CASE;

        ASSERT is_digit = true
        REPORT "wrong digit at t=" & TIME'IMAGE(NOW) &
            "seven_segment_out=" & to_string(seven_segment_out)
            SEVERITY FAILURE;

        CASE common_pins_out IS
            WHEN "0001" => common_pin := true; -- 1
            WHEN "0010" => common_pin := true; -- 2
            WHEN "0100" => common_pin := true; -- 3
            WHEN "1000" => common_pin := true; -- 4
            WHEN OTHERS => common_pin := false;
        END CASE;

        ASSERT common_pin = true
        REPORT "not a correct pin at t=" & TIME'IMAGE(NOW) &
            "common_pins_out=" & to_string(common_pins_out)
            SEVERITY FAILURE;

        IF (button_count > COUNT_TO) THEN
            REPORT "No error found. button_count = " & to_string(button_count);
            std.env.finish;
        END IF;
    END PROCESS;
END ARCHITECTURE;