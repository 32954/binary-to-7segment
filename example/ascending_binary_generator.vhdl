LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;

ENTITY ascending_binary_generator IS
    GENERIC (
        BINARY_WIDTH : POSITIVE;
        COUNT_TO : NATURAL
    );
    PORT (
        reset_n, count_clock : IN STD_LOGIC;
        binary_out : OUT STD_LOGIC_VECTOR((BINARY_WIDTH - 1) DOWNTO 0)
    );
END ascending_binary_generator;

ARCHITECTURE rtl OF ascending_binary_generator IS
    SIGNAL binary_value : STD_LOGIC_VECTOR((BINARY_WIDTH - 1) DOWNTO 0);
BEGIN
    PROCESS (count_clock, reset_n)
    BEGIN
        IF reset_n = '0' THEN
            binary_value <= (OTHERS => '0');
        ELSIF rising_edge(count_clock) THEN
            IF (binary_value < COUNT_TO) THEN
                binary_value <= binary_value + 1;
            ELSE
                binary_value <= (OTHERS => '0');
            END IF;
        END IF;
    END PROCESS;
    binary_out <= binary_value;
END ARCHITECTURE;