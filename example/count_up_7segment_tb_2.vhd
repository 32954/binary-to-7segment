LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY count_up_7segment_tb_2 IS
    CONSTANT DEBOUNCE_CLOCK_DIVIDER : POSITIVE := 1;
    CONSTANT MULTIPLEXING_CLOCK_DIVIDER : POSITIVE := 1;
    CONSTANT BINARY_WIDTH : POSITIVE := 14;
    CONSTANT COUNT_TO : NATURAL := 9999;
    CONSTANT DISPLAY_NUMBER : POSITIVE := 4;
    CONSTANT COMMON_ANODE : BOOLEAN := true;

    CONSTANT CLOCK_PERIOD : TIME := 20 ns;
    CONSTANT RESET_DURATION : TIME := 100 ns;
    CONSTANT PUSH_BUTTON_PERIOD : TIME := 200 ns;
    CONSTANT TRANSITION_DELAY : TIME := 1 ns;
END ENTITY;

ARCHITECTURE testbench OF count_up_7segment_tb_2 IS
    SIGNAL reset_n : STD_LOGIC := '0';
    SIGNAL push_button : STD_LOGIC := '0';
    SIGNAL clock : STD_LOGIC := '0';
    SIGNAL common_pins_out : STD_LOGIC_VECTOR ((DISPLAY_NUMBER - 1) DOWNTO 0);
    SIGNAL seven_segment_out : STD_LOGIC_VECTOR(6 DOWNTO 0);

    SIGNAL button_count : NATURAL;
    SIGNAL seven_segment_check : STD_LOGIC_VECTOR(6 DOWNTO 0);

    TYPE test_data_type IS RECORD
        digit : NATURAL RANGE 0 TO 9;
        seven_segment : STD_LOGIC_VECTOR(6 DOWNTO 0);
    END RECORD;

    TYPE test_data_array_type IS ARRAY (NATURAL RANGE <>) OF test_data_type;

    CONSTANT TEST_DATA : test_data_array_type :=
    (
    (0, "1000000"),
    (1, "1111001"),
    (2, "0100100"),
    (3, "0110000"),
    (4, "0011001"),
    (5, "0010010"),
    (6, "0000010"),
    (7, "1111000"),
    (8, "0000000"),
    (9, "0010000")
    );
BEGIN
    ---------------------------- DUT instantiation ----------------------------
    dut : ENTITY work.count_up_7segment GENERIC MAP (
        DEBOUNCE_CLOCK_DIVIDER => DEBOUNCE_CLOCK_DIVIDER,
        MULTIPLEXING_CLOCK_DIVIDER => MULTIPLEXING_CLOCK_DIVIDER,
        BINARY_WIDTH => BINARY_WIDTH,
        COUNT_TO => COUNT_TO,
        DISPLAY_NUMBER => DISPLAY_NUMBER,
        COMMON_ANODE => COMMON_ANODE
        )
        PORT MAP(
            reset_n => reset_n,
            push_button => push_button,
            clock => clock,
            common_pins_out => common_pins_out,
            seven_segment_out => seven_segment_out
        );
    --------------------------- stimuli generation ----------------------------
    clock <= NOT clock AFTER CLOCK_PERIOD / 2;
    reset_n <= '0', '1' AFTER RESET_DURATION;
    push_button <= NOT push_button AFTER PUSH_BUTTON_PERIOD;

    PROCESS
        ALIAS push_button_debounce IS << SIGNAL dut.debounced_push_button : STD_LOGIC >> ;
    BEGIN
        WAIT UNTIL rising_edge(push_button_debounce);
        button_count <= button_count + 1;
    END PROCESS;
    ---------------------------------- check ----------------------------------
    PROCESS
        VARIABLE digit, digit_4th, digit_3rd, digit_2nd, digit_1st : NATURAL;
    BEGIN
        IF (button_count > COUNT_TO) THEN
            REPORT "No error found. button_count = " & to_string(button_count);
            std.env.finish;
        END IF;

        digit_4th := button_count/1000 MOD 10;
        digit_3rd := button_count/100 MOD 10;
        digit_2nd := button_count/10 MOD 10;
        digit_1st := button_count MOD 10;

        CASE common_pins_out IS
            WHEN "0001" => digit := digit_1st; -- 1
            WHEN "0010" => digit := digit_2nd; -- 2
            WHEN "0100" => digit := digit_3rd; -- 3
            WHEN "1000" => digit := digit_4th; -- 4
            WHEN OTHERS => digit := 0;
        END CASE;

        WAIT UNTIL seven_segment_out'stable(TRANSITION_DELAY);
        seven_segment_check <= TEST_DATA(digit).seven_segment;
        ASSERT TEST_DATA(digit).seven_segment = seven_segment_out
        REPORT "wrong segment at t=" & TIME'IMAGE(NOW) &
            " seven_segment_out=" & to_string(seven_segment_out) &
            " TEST_DATA 7segment=" & to_string(TEST_DATA(digit).seven_segment)
            SEVERITY FAILURE;

        WAIT UNTIL seven_segment_out'event;
    END PROCESS;
END ARCHITECTURE;