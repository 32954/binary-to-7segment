LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY push_button_debounce IS
    PORT (
        reset_n, clock : IN STD_LOGIC;
        push_button : IN STD_LOGIC;
        debounced_push_button_out : OUT STD_LOGIC
    );
END push_button_debounce;

ARCHITECTURE RTL OF push_button_debounce IS
    SIGNAL delay1, delay2, delay3 : STD_LOGIC;
BEGIN
    PROCESS (clock, reset_n)
    BEGIN
        IF reset_n = '0' THEN
            delay1 <= '0';
            delay2 <= '0';
            delay3 <= '0';
        ELSIF rising_edge(clock) THEN
            delay1 <= push_button;
            delay2 <= delay1;
            delay3 <= delay2;
        END IF;
    END PROCESS;

    debounced_push_button_out <= delay1 AND delay2 AND delay3;
END ARCHITECTURE;