LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE work.count_up_7segment_pkg.ALL;

ENTITY count_up_7segment IS
    GENERIC (
        DEBOUNCE_CLOCK_DIVIDER : POSITIVE;
        MULTIPLEXING_CLOCK_DIVIDER : POSITIVE;
        BINARY_WIDTH : POSITIVE;
        COUNT_TO : NATURAL;
        DISPLAY_NUMBER : POSITIVE;
        COMMON_ANODE : BOOLEAN
    );
    PORT (
        reset_n, push_button, clock : IN STD_LOGIC;
        common_pins_out : OUT STD_LOGIC_VECTOR ((DISPLAY_NUMBER - 1) DOWNTO 0);
        seven_segment_out : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
    );
END ENTITY;

ARCHITECTURE structural OF count_up_7segment IS
    SIGNAL debounce_clock, debounced_push_button, multiplexing_clock : STD_LOGIC;
    SIGNAL binary_signal : STD_LOGIC_VECTOR((BINARY_WIDTH - 1) DOWNTO 0);
BEGIN
    ---------------------------------------------------------------------------
    debounce_clk : debounce_clock_generator GENERIC MAP(
        DEBOUNCE_CLOCK_DIVIDER => DEBOUNCE_CLOCK_DIVIDER
    )
    PORT MAP(
        reset_n => reset_n,
        clock => clock,
        debounce_clock_out => debounce_clock
    );
    ---------------------------------------------------------------------------
    button_debounce : push_button_debounce PORT MAP(
        reset_n => reset_n,
        clock => debounce_clock,
        push_button => push_button,
        debounced_push_button_out => debounced_push_button
    );
    ---------------------------------------------------------------------------
    binary_gen : ascending_binary_generator GENERIC MAP(
        BINARY_WIDTH => BINARY_WIDTH,
        COUNT_TO => COUNT_TO
    )
    PORT MAP(
        reset_n => reset_n,
        count_clock => debounced_push_button,
        binary_out => binary_signal
    );
    ---------------------------------------------------------------------------
    multiplexing_clk : multiplexing_clock_generator GENERIC MAP(
        MULTIPLEXING_CLOCK_DIVIDER => MULTIPLEXING_CLOCK_DIVIDER
    )
    PORT MAP(
        reset_n => reset_n,
        clock => clock,
        multiplexing_clock_out => multiplexing_clock
    );
    ---------------------------------------------------------------------------
    module_7segment : binary_to_7segment GENERIC MAP(
        BINARY_WIDTH => BINARY_WIDTH,
        DISPLAY_NUMBER => DISPLAY_NUMBER,
        COMMON_ANODE => COMMON_ANODE
    )
    PORT MAP(
        reset_n => reset_n,
        multiplexing_clock => multiplexing_clock,
        binary_input => binary_signal,
        common_pins_out => common_pins_out,
        seven_segment_out => seven_segment_out
    );
END ARCHITECTURE;