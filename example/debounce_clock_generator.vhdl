LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;

ENTITY debounce_clock_generator IS
    GENERIC (
        DEBOUNCE_CLOCK_DIVIDER : POSITIVE
    );
    PORT (
        reset_n, clock : IN STD_LOGIC;
        debounce_clock_out : OUT STD_LOGIC
    );
END debounce_clock_generator;

ARCHITECTURE rtl OF debounce_clock_generator IS
    SIGNAL debounce : STD_LOGIC_VECTOR((DEBOUNCE_CLOCK_DIVIDER - 1) DOWNTO 0);
BEGIN
    ---------------------------------------------------------------------------
    PROCESS (clock, reset_n)
    BEGIN
        IF reset_n = '0' THEN
            debounce <= (OTHERS => '0');
        ELSIF rising_edge(clock) THEN
            debounce <= debounce + 1;
        END IF;
    END PROCESS;
    debounce_clock_out <= debounce(DEBOUNCE_CLOCK_DIVIDER - 1);
END ARCHITECTURE;