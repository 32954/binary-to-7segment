LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;

ENTITY multiplexing_clock_generator IS
    GENERIC (
        MULTIPLEXING_CLOCK_DIVIDER : POSITIVE
    );
    PORT (
        reset_n, clock : IN STD_LOGIC;
        multiplexing_clock_out : OUT STD_LOGIC
    );
END multiplexing_clock_generator;

ARCHITECTURE rtl OF multiplexing_clock_generator IS
    SIGNAL refresh : STD_LOGIC_VECTOR((MULTIPLEXING_CLOCK_DIVIDER - 1) DOWNTO 0);
BEGIN
    ---------------------------------------------------------------------------
    PROCESS (clock, reset_n)
    BEGIN
        IF reset_n = '0' THEN
            refresh <= (OTHERS => '0');
        ELSIF rising_edge(clock) THEN
            refresh <= refresh + 1;
        END IF;
    END PROCESS;
    multiplexing_clock_out <= refresh(MULTIPLEXING_CLOCK_DIVIDER - 1);
END ARCHITECTURE;