LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

PACKAGE binary_to_7segment_pkg IS
    COMPONENT binary_to_bcd IS
        GENERIC (
            BINARY_WIDTH : POSITIVE;
            TOTAL_BCD_DIGIT : POSITIVE
        );
        PORT (
            binary_in : IN STD_LOGIC_VECTOR ((BINARY_WIDTH - 1) DOWNTO 0);
            bcd_out : OUT STD_LOGIC_VECTOR(((TOTAL_BCD_DIGIT * 4) - 1) DOWNTO 0)
        );
    END COMPONENT;
    ---------------------------------------------------------------------------
    COMPONENT generic_mux IS
        GENERIC (
            N_BIT_MUX : POSITIVE;
            TOTAL_MUX_INPUT : POSITIVE
        );
        PORT (
            mux_input : IN STD_LOGIC_VECTOR(((TOTAL_MUX_INPUT * N_BIT_MUX) - 1) DOWNTO 0);
            mux_select : IN NATURAL RANGE (TOTAL_MUX_INPUT - 1) DOWNTO 0;
            mux_output : OUT STD_LOGIC_VECTOR((N_BIT_MUX - 1) DOWNTO 0)
        );
    END COMPONENT;
    ---------------------------------------------------------------------------
    COMPONENT bcd_to_7segment IS
        GENERIC (
            COMMON_ANODE : BOOLEAN
        );
        PORT (
            bcd_digit_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            seven_segment_out : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
        );
    END COMPONENT;
    ---------------------------------------------------------------------------
    COMPONENT common_pin_selector IS
        GENERIC (
            DISPLAY_NUMBER : POSITIVE;
            COMMON_ANODE : BOOLEAN
        );
        PORT (
            reset_n, multiplexing_clock : IN STD_LOGIC;
            mux_select : OUT NATURAL RANGE (DISPLAY_NUMBER - 1) DOWNTO 0;
            common_pins_out : OUT STD_LOGIC_VECTOR ((DISPLAY_NUMBER - 1) DOWNTO 0)
        );
    END COMPONENT;
END PACKAGE;