LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY generic_mux IS
    GENERIC (
        N_BIT_MUX : POSITIVE;
        TOTAL_MUX_INPUT : POSITIVE
    );
    PORT (
        mux_input : IN STD_LOGIC_VECTOR(((TOTAL_MUX_INPUT * N_BIT_MUX) - 1) DOWNTO 0);
        mux_select : IN NATURAL RANGE (TOTAL_MUX_INPUT - 1) DOWNTO 0;
        mux_output : OUT STD_LOGIC_VECTOR((N_BIT_MUX - 1) DOWNTO 0)
    );
END ENTITY;

ARCHITECTURE behavioral OF generic_mux IS

    TYPE generic_mux_type IS ARRAY (NATURAL RANGE <>) OF STD_LOGIC_VECTOR;
    ---------------------------------------------------------------------------
    SIGNAL multiplexer : generic_mux_type((TOTAL_MUX_INPUT - 1) DOWNTO 0)((N_BIT_MUX - 1) DOWNTO 0);
    ---------------------------------------------------------------------------
    FUNCTION slv_to_generic_mux_type(
        slv : STD_LOGIC_VECTOR;
        TOTAL_MUX_INPUT : NATURAL;
        N_BIT_MUX : NATURAL
    )
        RETURN generic_mux_type IS
        VARIABLE multiplexer : generic_mux_type((TOTAL_MUX_INPUT - 1) DOWNTO 0)((N_BIT_MUX - 1) DOWNTO 0);
    BEGIN
        FOR i IN 1 TO TOTAL_MUX_INPUT LOOP
            multiplexer(i - 1)
            := slv(((i * N_BIT_MUX) - 1) DOWNTO ((i * N_BIT_MUX) - N_BIT_MUX));
        END LOOP;
        RETURN multiplexer;
    END FUNCTION;

BEGIN

    multiplexer <= slv_to_generic_mux_type(mux_input, TOTAL_MUX_INPUT, N_BIT_MUX);
    mux_output <= multiplexer(mux_select);

END ARCHITECTURE;