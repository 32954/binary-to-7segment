LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY bcd_to_7segment IS
    GENERIC (
        COMMON_ANODE : BOOLEAN
    );
    PORT (
        bcd_digit_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        seven_segment_out : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
    );
END bcd_to_7segment;

ARCHITECTURE rtl OF bcd_to_7segment IS
    SIGNAL seven_segment_digit : STD_LOGIC_VECTOR(6 DOWNTO 0);
BEGIN

    WITH bcd_digit_in SELECT
        seven_segment_digit <=
        --G_F_E_D_C_B_A
        b"1_0_0_0_0_0_0" WHEN "0000", -- 1000000 / 0
        b"1_1_1_1_0_0_1" WHEN "0001", -- 1111001 / 1
        b"0_1_0_0_1_0_0" WHEN "0010", -- 0100100 / 2
        b"0_1_1_0_0_0_0" WHEN "0011", -- 0110000 / 3
        b"0_0_1_1_0_0_1" WHEN "0100", -- 0011001 / 4
        b"0_0_1_0_0_1_0" WHEN "0101", -- 0010010 / 5
        b"0_0_0_0_0_1_0" WHEN "0110", -- 0000010 / 6
        b"1_1_1_1_0_0_0" WHEN "0111", -- 1111000 / 7
        b"0_0_0_0_0_0_0" WHEN "1000", -- 0000000 / 8
        b"0_0_1_0_0_0_0" WHEN "1001", -- 0010000 / 9
        b"1_1_1_1_1_1_1" WHEN OTHERS; -- 1111111 / blank when not a digit

    seven_segment_out <= seven_segment_digit WHEN COMMON_ANODE = true ELSE
        NOT seven_segment_digit;
END ARCHITECTURE;