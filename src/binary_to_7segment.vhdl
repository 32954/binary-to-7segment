LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE work.binary_to_7segment_pkg.ALL;

ENTITY binary_to_7segment IS
    GENERIC (
        BINARY_WIDTH : POSITIVE;
        DISPLAY_NUMBER : POSITIVE;
        COMMON_ANODE : BOOLEAN
    );
    PORT (
        reset_n, multiplexing_clock : IN STD_LOGIC;
        binary_input : IN STD_LOGIC_VECTOR((BINARY_WIDTH - 1) DOWNTO 0);
        common_pins_out : OUT STD_LOGIC_VECTOR ((DISPLAY_NUMBER - 1) DOWNTO 0);
        seven_segment_out : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
    );
END ENTITY;

ARCHITECTURE structural OF binary_to_7segment IS
    CONSTANT BCD_DIGIT_WIDTH : POSITIVE := 4;
    ---------------------------------------------------------------------------
    SIGNAL binary_bcd_to_mux : STD_LOGIC_VECTOR(((BCD_DIGIT_WIDTH * DISPLAY_NUMBER) - 1) DOWNTO 0);
    ---------------------------------------------------------------------------
    SIGNAL mux_select_signal : NATURAL RANGE (DISPLAY_NUMBER - 1) DOWNTO 0;
    ---------------------------------------------------------------------------
    SIGNAL mux_to_bcd_7segment : STD_LOGIC_VECTOR((BCD_DIGIT_WIDTH - 1) DOWNTO 0);
BEGIN
    ---------------------------------------------------------------------------
    module_binary_to_bcd : binary_to_bcd GENERIC MAP(
        BINARY_WIDTH => BINARY_WIDTH,
        TOTAL_BCD_DIGIT => DISPLAY_NUMBER
    )
    PORT MAP(
        binary_in => binary_input,
        bcd_out => binary_bcd_to_mux
    );
    ---------------------------------------------------------------------------
    module_generic_mux : generic_mux GENERIC MAP(
        N_BIT_MUX => BCD_DIGIT_WIDTH,
        TOTAL_MUX_INPUT => DISPLAY_NUMBER
    )
    PORT MAP(
        mux_input => binary_bcd_to_mux,
        mux_select => mux_select_signal,
        mux_output => mux_to_bcd_7segment
    );
    ---------------------------------------------------------------------------
    module_bcd_to_7segment : bcd_to_7segment GENERIC MAP(
        COMMON_ANODE => COMMON_ANODE
    )
    PORT MAP(
        bcd_digit_in => mux_to_bcd_7segment,
        seven_segment_out => seven_segment_out
    );
    ---------------------------------------------------------------------------
    module_common_pin_selector : common_pin_selector GENERIC MAP(
        DISPLAY_NUMBER => DISPLAY_NUMBER,
        COMMON_ANODE => COMMON_ANODE
    )
    PORT MAP(
        reset_n => reset_n, multiplexing_clock => multiplexing_clock,
        mux_select => mux_select_signal,
        common_pins_out => common_pins_out
    );
END ARCHITECTURE;