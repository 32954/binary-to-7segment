LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY common_pin_selector IS
    GENERIC (
        DISPLAY_NUMBER : POSITIVE;
        COMMON_ANODE : BOOLEAN
    );
    PORT (
        reset_n, multiplexing_clock : IN STD_LOGIC;
        mux_select : OUT NATURAL RANGE (DISPLAY_NUMBER - 1) DOWNTO 0;
        common_pins_out : OUT STD_LOGIC_VECTOR ((DISPLAY_NUMBER - 1) DOWNTO 0)
    );
END common_pin_selector;

ARCHITECTURE rtl OF common_pin_selector IS
    SIGNAL common_pin_pos : STD_LOGIC_VECTOR((DISPLAY_NUMBER - 1) DOWNTO 0);
BEGIN
    generic_mux_select : PROCESS (multiplexing_clock, reset_n)
        VARIABLE select_value : NATURAL RANGE (DISPLAY_NUMBER - 1) DOWNTO 0;
    BEGIN
        IF reset_n = '0' THEN
            select_value := 0;
        ELSIF rising_edge(multiplexing_clock) THEN
            IF (select_value < (DISPLAY_NUMBER - 1)) THEN
                select_value := select_value + 1;
            ELSE
                select_value := 0;
            END IF;
        END IF;

        mux_select <= select_value;
    END PROCESS;
    ---------------------------------------------------------------------------
    common_pin_ring_counter : PROCESS (multiplexing_clock, reset_n)
    BEGIN
        IF reset_n = '0' THEN
            common_pin_pos <= ((DISPLAY_NUMBER - 1) DOWNTO 1 => '0') & '1';
        ELSIF rising_edge(multiplexing_clock) THEN
            common_pin_pos(0) <= common_pin_pos(DISPLAY_NUMBER - 1);
            common_pin_pos((DISPLAY_NUMBER - 1) DOWNTO 1) <= common_pin_pos((DISPLAY_NUMBER - 2) DOWNTO 0);
        END IF;
    END PROCESS;

    common_pins_out <= common_pin_pos WHEN COMMON_ANODE = true ELSE
        NOT common_pin_pos;
END ARCHITECTURE;