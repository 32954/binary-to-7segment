LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;

ENTITY binary_to_bcd IS
    GENERIC (
        BINARY_WIDTH : POSITIVE;
        TOTAL_BCD_DIGIT : POSITIVE
    );
    PORT (
        binary_in : IN STD_LOGIC_VECTOR ((BINARY_WIDTH - 1) DOWNTO 0) := (OTHERS => '0');
        bcd_out : OUT STD_LOGIC_VECTOR(((TOTAL_BCD_DIGIT * 4) - 1) DOWNTO 0)
    );
END binary_to_bcd;

ARCHITECTURE double_dabble OF binary_to_bcd IS
BEGIN
    PROCESS (binary_in)
        VARIABLE binary_value : STD_LOGIC_VECTOR ((BINARY_WIDTH - 1) DOWNTO 0);
        VARIABLE bcd_value : STD_LOGIC_VECTOR (((TOTAL_BCD_DIGIT * 4) - 1) DOWNTO 0);
    BEGIN
        bcd_value := (OTHERS => '0');
        binary_value := binary_in;

        FOR i IN 0 TO BINARY_WIDTH - 1 LOOP
            FOR i IN 1 TO TOTAL_BCD_DIGIT LOOP
                IF bcd_value((4 * i - 1) DOWNTO (4 * (i - 1))) > 4 THEN
                    bcd_value((4 * i - 1) DOWNTO (4 * (i - 1))) :=
                    bcd_value((4 * i - 1) DOWNTO (4 * (i - 1))) + 3;
                END IF;
            END LOOP;
            -- shift bcd_value left by 1 bit, copy MSB of binary_value into LSB of bcd_value
            bcd_value := bcd_value(((TOTAL_BCD_DIGIT * 4) - 2) DOWNTO 0) & binary_value(BINARY_WIDTH - 1);
            -- shift binary_value left by 1 bit
            binary_value := binary_value((BINARY_WIDTH - 2) DOWNTO 0) & '0';
        END LOOP;

        bcd_out <= bcd_value;
    END PROCESS;
END ARCHITECTURE;